#include <iostream>
#include <string>
#define Min 0.000001
using namespace std;

struct monomial
{
	int sobien = 1;
	float coe;
	char *var;
	unsigned short *exp;
	monomial *pNext;
	monomial()
	{}
	monomial(monomial &a)
	{
		coe = a.coe;
		exp = a.exp;
		var = a.var;
		sobien = a.sobien;
	}
	monomial &operator=(monomial a)
	{
		monomial tmp(a);
		tmp.pNext = a.pNext;
		return tmp;
	}
	monomial operator+(monomial a)
	{
		monomial tmp;
		tmp.coe = coe + a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator-(monomial a)
	{
		monomial tmp;
		tmp.coe = coe - a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator*(monomial a)
	{
		monomial tmp = a; // luu a 
		tmp.coe *= coe;
		int m, n;
		m = strlen(a.var);
		n = strlen(var);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
				if (a.var[i] == var[j]) // gap bien giong nhau 
				{
					tmp.exp[i] += exp[j]; // cap nhat mu 
				}
		}
		int count = -1;
		for (int i = 0; i < n; i++)
		{
			bool test = false;
			for (int j = 0; j < m; j++)
				if (var[i] == a.var[j])
					test = true; // bien cua b da ton tai trong don thuc moi 
			if (!test) // khong co bien cua b trong don thuc moi  
			{
				count++;
				tmp.var[m + count] = var[i];
				tmp.exp[m + count] = exp[i];
			}
		}
		return tmp;
	}
};

class Polymial
{
public:
	void shorten();//shorten polynomial by deleting node has coe=0
private:
	monomial *pHead;
	monomial *pTail;
	int compare(monomial T, monomial P);
};

int Polymial::compare(monomial T, monomial P)
{
	int n = strlen(T.var);
	int m = strlen(P.var);

	int temp1 = 0, temp2 = 0;

	for (int i = 0; i < n; i++)
		temp1 = temp1 + T.exp[i] * T.var[i];

	for (int j = 0; j < m; j++)
		temp2 = temp2 + P.exp[j] * P.var[j];
	if (temp1 == temp2) return 0;
	return -1;
}
//shorten polynomial by deleting node has coe=0
void Polymial::shorten()
{
	if (pHead == NULL)
		return;
	monomial *tmp = pHead;

	while (tmp != NULL)
	{
		if (tmp->coe != 0) // cap nhat he so cho nhung don thuc co bien giong nhau 
		{
			monomial *tmp2 = tmp->pNext;
			while (tmp2 != NULL)
			{
				if (compare(*tmp, *tmp2) == 0) // Da xu li 3 truong hop (a^2*b , b*a^2 ) va ( a^2*b^2 , b^2*a^2) va ( a^2*b^2 , a^2*b^2)
				{
					tmp->coe = tmp->coe + tmp2->coe;
					cout << tmp->coe << endl;
					tmp2->coe = 0;
				}
				tmp2 = tmp2->pNext;
			}
		}
		tmp = tmp->pNext;
	}
	//Delete coe = 0
	tmp = pHead;
	monomial *tmp2 = tmp->pNext;
	if (fabs(tmp->coe) < Min)
	{
		pHead = pHead->pNext;
		delete tmp;
		tmp = pHead;
	}
	while (tmp2 != NULL)
	{
		if (fabs(tmp2->coe) < Min)
		{
			monomial *tmp_del = tmp2;
			tmp2 = tmp2->pNext;
			tmp->pNext = tmp2;
			delete tmp_del;
		}
		else
		{
			tmp = tmp2;
			tmp2 = tmp2->pNext;
		}
	}
}